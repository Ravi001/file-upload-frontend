import {
    applyMiddleware, combineReducers, compose, createStore,
} from 'redux';
import thunk from 'redux-thunk';
import {
    abortMultipartUploadReducer,
    completeMultipartUploadReducer,
    continueMultipartUploadReducer,
    initiateMultipartUploadReducer,
} from './reducers/files';

const reducers = combineReducers({
    initiateMultipartUpload: initiateMultipartUploadReducer,
    continueMultipartUpload: continueMultipartUploadReducer,
    completeMultipartUpload: completeMultipartUploadReducer,
    abortMultipartUpload: abortMultipartUploadReducer,
});

const initialState = {};
const middleware = [thunk];
const store = createStore(reducers, initialState, compose(applyMiddleware(...middleware)));

export default store;
