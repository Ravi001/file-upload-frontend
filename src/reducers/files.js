import {
    FILE_UPLOAD_ABORT_FAIL,
    FILE_UPLOAD_ABORT_REQUEST,
    FILE_UPLOAD_ABORT_SUCCESS,
    FILE_UPLOAD_FAIL,
    FILE_UPLOAD_FINISH_FAIL,
    FILE_UPLOAD_FINISH_REQUEST, FILE_UPLOAD_FINISH_SUCCESS,
    FILE_UPLOAD_REQUEST, FILE_UPLOAD_START_FAIL,
    FILE_UPLOAD_START_REQUEST, FILE_UPLOAD_START_SUCCESS, FILE_UPLOAD_SUCCESS,
} from '../constants/files';

export const initiateMultipartUploadReducer = (state = { }, action) => {
    switch (action.type) {
    case FILE_UPLOAD_START_REQUEST:
        return { ...state, loading: true };
    case FILE_UPLOAD_START_SUCCESS:
        return {
            ...state, loading: false, data: action.payload,
        };
    case FILE_UPLOAD_START_FAIL:
        return { ...state, loading: false, error: action.payload };
    default:
        return state;
    }
};

export const continueMultipartUploadReducer = (state = { }, action) => {
    switch (action.type) {
    case FILE_UPLOAD_REQUEST:
        return { ...state, loading: true };
    case FILE_UPLOAD_SUCCESS:
        return {
            ...state, loading: false, data: action.payload,
        };
    case FILE_UPLOAD_FAIL:
        return { ...state, loading: false, error: action.payload };
    default:
        return state;
    }
};

export const completeMultipartUploadReducer = (state = { }, action) => {
    switch (action.type) {
    case FILE_UPLOAD_FINISH_REQUEST:
        return { ...state, loading: true };
    case FILE_UPLOAD_FINISH_SUCCESS:
        return {
            ...state, loading: false, data: action.payload,
        };
    case FILE_UPLOAD_FINISH_FAIL:
        return { ...state, loading: false, error: action.payload };
    default:
        return state;
    }
};

export const abortMultipartUploadReducer = (state = { }, action) => {
    switch (action.type) {
    case FILE_UPLOAD_ABORT_REQUEST:
        return { ...state, loading: true };
    case FILE_UPLOAD_ABORT_SUCCESS:
        return {
            ...state, loading: false, data: action.payload,
        };
    case FILE_UPLOAD_ABORT_FAIL:
        return { ...state, loading: false, error: action.payload };
    default:
        return state;
    }
};
