import axios from 'axios';
import {
    FILE_UPLOAD_ABORT_FAIL,
    FILE_UPLOAD_ABORT_REQUEST,
    FILE_UPLOAD_ABORT_SUCCESS,
    FILE_UPLOAD_FAIL, FILE_UPLOAD_FINISH_FAIL, FILE_UPLOAD_FINISH_REQUEST, FILE_UPLOAD_FINISH_SUCCESS, FILE_UPLOAD_REQUEST,
    FILE_UPLOAD_START_FAIL, FILE_UPLOAD_START_REQUEST,
    FILE_UPLOAD_START_SUCCESS, FILE_UPLOAD_SUCCESS,
} from '../constants/files';

export const initiateMultipartUpload = (name) => async (dispatch) => {
    try {
        dispatch({ type: FILE_UPLOAD_START_REQUEST });
        const { data } = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/v1/files/start`, { name });
        dispatch({ type: FILE_UPLOAD_START_SUCCESS, payload: data.data });
    } catch (error) {
        dispatch({
            type: FILE_UPLOAD_START_FAIL,
            payload: error.response && error.response.data?.message ? error.response.data?.message : error.message,
        });
    }
};

export const continueMultipartUpload = (file, uploadId, partNumber, fileName) => async (dispatch) => {
    try {
        dispatch({ type: FILE_UPLOAD_REQUEST });
        const form = new FormData();
        form.append('file', file);
        form.append('uploadId', uploadId);
        form.append('partNumber', partNumber);
        form.append('name', fileName);
        const { data } = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/v1/files/upload`, form);
        dispatch({ type: FILE_UPLOAD_SUCCESS, payload: data.data });
    } catch (error) {
        dispatch({
            type: FILE_UPLOAD_FAIL,
            payload: error.response && error.response.data?.message ? error.response.data?.message : error.message,
        });
    }
};

export const completeMultipartUpload = (uploadId, parts, name) => async (dispatch) => {
    try {
        dispatch({ type: FILE_UPLOAD_FINISH_REQUEST });
        const { data } = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/v1/files/finish`, { uploadId, parts, name });
        dispatch({ type: FILE_UPLOAD_FINISH_SUCCESS, payload: data.data });
    } catch (error) {
        dispatch({
            type: FILE_UPLOAD_FINISH_FAIL,
            payload: error.response && error.response.data?.message ? error.response.data?.message : error.message,
        });
    }
};

export const abortMultipartUpload = (uploadId, name) => async (dispatch) => {
    try {
        dispatch({ type: FILE_UPLOAD_ABORT_REQUEST });
        const { data } = await axios.post(`${process.env.REACT_APP_API_BASE_URL}/v1/files/abort`, { uploadId, name });
        dispatch({ type: FILE_UPLOAD_ABORT_SUCCESS, payload: data.data });
    } catch (error) {
        dispatch({
            type: FILE_UPLOAD_ABORT_FAIL,
            payload: error.response && error.response.data?.message ? error.response.data?.message : error.message,
        });
    }
};
