/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useRef, useState } from 'react';
import { FileUpload } from 'primereact/fileupload';
import { Button } from 'primereact/button';
import { Tag } from 'primereact/tag';
import { Toast } from 'primereact/toast';
import { ProgressBar } from 'primereact/progressbar';
import { useDispatch, useSelector } from 'react-redux';
import {
    abortMultipartUpload, completeMultipartUpload, continueMultipartUpload, initiateMultipartUpload,
} from '../actions/files';
import { FILE_UPLOAD_FAIL, FILE_UPLOAD_FINISH_FAIL, FILE_UPLOAD_FINISH_SUCCESS } from '../constants/files';

const chunkSize = 1048 * 1048 * 6;

const HomeScreen = () => {
    const toast = useRef(null);
    const [showProgress, setShowProgress] = useState(false);
    const [startUpload, setStartUpload] = useState(false);
    const [progress, setProgress] = useState(0);
    const [uploadId, setUploadId] = useState('');
    const [fileState, setFileState] = useState({
        fileName: '',
        fileSize: 0,
        totalChunks: 0,
        totalChunksUploaded: 0,
        startChunk: 0,
        endChunk: chunkSize,
        fileToUpload: null,
        uploadedBytes: 0,
        partNumber: 1,
    });
    const [parts, setParts] = useState([]);

    const fileUploadRef = useRef(null);
    const dispatch = useDispatch();
    const { data } = useSelector((state) => state.initiateMultipartUpload);
    const { data: continueData, error: continueError } = useSelector((state) => state.continueMultipartUpload);
    const { data: completeData, error: completeError } = useSelector((state) => state.completeMultipartUpload);

    const resetState = () => {
        setFileState({
            fileName: '',
            fileSize: 0,
            totalChunks: 0,
            totalChunksUploaded: 0,
            startChunk: 0,
            endChunk: chunkSize,
            fileToUpload: null,
            uploadedBytes: 0,
            partNumber: 1,
        });
    };

    const chunkedUpload = (totalChunksUploaded) => {
        if (totalChunksUploaded <= fileState.totalChunks) {
            const chunk = fileState.fileToUpload.slice(fileState.startChunk, fileState.endChunk);
            dispatch(continueMultipartUpload(chunk, uploadId, fileState.partNumber, fileState.fileName));
            const endingChunk = Math.min(fileState.endChunk + chunkSize, fileState.fileSize);
            setFileState((prev) => ({
                ...prev,
                totalChunksUploaded: prev.totalChunksUploaded + 1,
                startChunk: prev.endChunk,
                endChunk: endingChunk === fileState.fileSize ? endingChunk + 1 : endingChunk,
                uploadedBytes: endingChunk,
                partNumber: prev.partNumber + 1,
            }));
            const progress = fileState.fileSize ? (fileState.uploadedBytes / fileState.fileSize) * 100 : 0.1;
            setProgress(progress);
        }
    };

    useEffect(() => {
        if (fileState.fileSize > 0 && startUpload && uploadId) {
            chunkedUpload(fileState.totalChunksUploaded);
            setStartUpload(false);
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [fileState.fileSize, uploadId, startUpload]);

    useEffect(() => {
        if (data) {
            setUploadId(data);
        }
    }, [data]);

    useEffect(() => {
        if (parts && continueData && (+continueData.partNumber === fileState.totalChunks + 1) && parts.length !== 0 && parts.length === fileState.totalChunks) {
            const sortedParts = parts.sort((a, b) => a.PartNumber - b.PartNumber);
            dispatch(completeMultipartUpload(uploadId, sortedParts, fileState.fileName));
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [parts, continueData]);

    useEffect(() => {
        if (completeData) {
            toast.current.show({
                severity: 'success',
                summary: 'File',
                detail: 'File Uploaded Successfully',
                life: 2000,
            });
            resetState();
            setShowProgress(false);
            setProgress(0);
            setUploadId('');
            setParts([]);
            dispatch({ type: FILE_UPLOAD_FINISH_SUCCESS, payload: null });
        }
    }, [completeData, dispatch]);

    useEffect(() => {
        if (continueError || completeError) {
            dispatch(abortMultipartUpload(uploadId, fileState.fileName));
            dispatch({ type: FILE_UPLOAD_FAIL, payload: null });
            dispatch({ type: FILE_UPLOAD_FINISH_FAIL, payload: null });
            toast.current.show({
                severity: 'error',
                summary: 'Failue',
                detail: continueError || completeError,
                life: 2000,
            });
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [continueError, completeError]);

    useEffect(() => {
        if (continueData && continueData.eTag && continueData.partNumber) {
            chunkedUpload(fileState.totalChunksUploaded);
            setParts((prev) => ([...prev, { ETag: continueData.eTag, PartNumber: +continueData.partNumber }]));
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [continueData]);

    const headerTemplate = (options) => {
        const {
            className, chooseButton, uploadButton, cancelButton,
        } = options;

        return (
            <div className={className} style={{ backgroundColor: 'transparent', display: 'flex', alignItems: 'center' }}>
                {chooseButton}
                {uploadButton}
                {cancelButton}
            </div>
        );
    };
    const itemTemplate = (file, props) => (
        <div className='flex align-items-center flex-wrap'>
            <div className='flex align-items-center' style={{ width: '60%' }}>
                <span className='flex flex-column text-left ml-3'>
                    {file.name}
                </span>
            </div>
            <Tag value={props.formatSize} severity='warning' className='px-3 py-2' />
            <Button
                type='button'
                icon='pi pi-times'
                className='p-button-outlined p-button-rounded p-button-danger ml-auto'
                onClick={props.onRemove}
            />
        </div>
    );

    const emptyTemplate = () => (
        <div className='flex align-items-center flex-column'>
            <i
                className='pi pi-file-excel mt-3 p-5'
                style={{
                    fontSize: '5em', borderRadius: '50%', backgroundColor: 'var(--surface-b)', color: 'var(--surface-d)',
                }}
            />
            <span style={{ fontSize: '1.2em', color: 'var(--text-color-secondary)' }} className='my-5'>Drag and Drop Csv Files</span>
        </div>
    );

    const chooseOptions = { icon: 'pi pi-fw pi-file', iconOnly: true, className: 'custom-choose-btn p-button-rounded p-button-outlined' };
    const uploadOptions = { icon: 'pi pi-fw pi-cloud-upload', iconOnly: true, className: 'custom-upload-btn p-button-success p-button-rounded p-button-outlined' };
    const cancelOptions = { icon: 'pi pi-fw pi-times', iconOnly: true, className: 'custom-cancel-btn p-button-danger p-button-rounded p-button-outlined' };

    const onFileUpload = (e) => {
        const file = e.files[0];
        setShowProgress(true);
        setProgress(0);
        resetState();
        dispatch(initiateMultipartUpload(file.name));
        setFileState((prev) => ({
            ...prev, fileSize: file.size, fileName: file.name, totalChunks: Math.floor(file.size / chunkSize), fileToUpload: file,
        }));
        setStartUpload(true);
    };

    return (
        <div className='App flex align-items-center justify-content-center'>
            <Toast ref={toast} />
            {showProgress ? (
                <div className='w-6'>
                    <ProgressBar value={progress} color='green' />
                </div>
            ) : (
                <div className='w-6'>
                    <FileUpload
                        className='shadow-2 border border-round-lg'
                        ref={fileUploadRef}
                        multiple
                        // accept='text/csv'
                        maxFileSize={10000000000}
                        headerTemplate={headerTemplate}
                        customUpload
                        uploadHandler={onFileUpload}
                        itemTemplate={itemTemplate}
                        emptyTemplate={emptyTemplate}
                        chooseOptions={chooseOptions}
                        uploadOptions={uploadOptions}
                        cancelOptions={cancelOptions}
                    />

                </div>
            )}

        </div>

    );
};

export default HomeScreen;
