import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PrimeReact from 'primereact/api';

import HomeScreen from './screens/HomeScreen';
import NotFound from './components/NotFound';

import 'primereact/resources/themes/lara-light-teal/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
// eslint-disable-next-line import/no-absolute-path
import '/node_modules/primeflex/primeflex.css';
import './styles/App.css';

PrimeReact.ripple = true;

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<HomeScreen />} />
                <Route
                    path='/'
                    element={(
                        <HomeScreen />
                    )}
                />
                <Route path='*' element={<NotFound />} />
            </Routes>
        </BrowserRouter>

    );
}

export default App;
