import React from 'react';

const NotFound = () => (
    <div className='full-body text-center'>
        <img src='/notFound.svg' alt='Not Found' style={{ height: '800px' }} />
    </div>
);

export default NotFound;
