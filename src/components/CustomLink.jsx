/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import PropTypes from 'prop-types';
import { Link, useMatch, useResolvedPath } from 'react-router-dom';

const CustomLink = ({ children, to, ...props }) => {
    const resolved = useResolvedPath(to);
    const match = useMatch({ path: resolved.pathname, end: true });
    const style = {
        color: 'var(--primary-color)',
    };

    return (
        <div>
            <Link style={match && style} to={to} {...props}>
                {children}
            </Link>
        </div>
    );
};

CustomLink.propTypes = {
    children: PropTypes.element.isRequired,
    to: PropTypes.string.isRequired,
};

export default CustomLink;
